/**
 * 
 */
package com.fouad.backbase.camel.processor;

import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import com.fouad.backbase.model.GeocodeResponse;

/**
 * @author Ahmed Fouad
 *
 * Processes the Geocode extracted data, and sets the body with the right model to the exchange
 *
 */
@Component
public class GeoCodeProcessor implements Processor {

	@Override
	public void process(Exchange exchange) throws Exception {
		GeocodeResponse geocodeResponse = new GeocodeResponse();
		Message message = exchange.getIn();
		geocodeResponse.setFormattedAddress(message.getHeader("formatted_address", String.class));
		geocodeResponse.setLatitude(message.getHeader("latitude", String.class));
		geocodeResponse.setLongitude(message.getHeader("longitude", String.class));
		exchange.getIn().setBody(geocodeResponse);
	}

}
