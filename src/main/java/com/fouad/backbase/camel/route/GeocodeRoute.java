/**
 * 
 */
package com.fouad.backbase.camel.route;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import com.fouad.backbase.camel.processor.GeoCodeProcessor;

/**
 * @author Ahmed Fouad
 * 
 * 
 * Invokes the google Geocode API, and routes the response for the appropriate processor 
 * 
 */
@Component
public class GeocodeRoute extends RouteBuilder {

	@Autowired
	private GeoCodeProcessor geocodeProcessor;
	
	@Override
	public void configure() throws Exception {

		from("direct:geoservice").routeId("direct-route").tracing().log("${in.header.address}")
				.toD("https4:maps.googleapis.com/maps/api/geocode/xml?address=${in.header.address}&key=AIzaSyCLYINSIV5P-1DpBuhlm3ImNaSyW9y9-Nk&bridgeEndpoint=true&throwExceptionOnFailure=false")
				.convertBodyTo(String.class).log("${body}").choice()
				.when(xpath("/GeocodeResponse/status/text() = 'OVER_QUERY_LIMIT'")).setBody(constant(null))
				.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(HttpStatus.TOO_MANY_REQUESTS.value()))
				.when(xpath("/GeocodeResponse/status/text() = 'ZERO_RESULTS'")).setBody(constant(null))
				.setHeader(Exchange.HTTP_RESPONSE_CODE, constant(HttpStatus.NOT_FOUND.value())).otherwise()
				.setHeader("formatted_address", xpath("/GeocodeResponse/result/formatted_address/text()"))
				.setHeader("latitude", xpath("/GeocodeResponse/result/geometry/location/lat/text()"))
				.setHeader("longitude", xpath("/GeocodeResponse/result/geometry/location/lng/text()"))
				.process(geocodeProcessor).marshal().json().end();
	}

}
