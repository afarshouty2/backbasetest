package com.fouad.backbase;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackbaseTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackbaseTestApplication.class, args);
	}
	
//	@Bean
//	@Autowired
//	public GeoApiContext geoApiContext() {
//		Builder geoApiContextBuilder = new GeoApiContext.Builder();
//		geoApiContextBuilder.apiKey("AIzaSyCLYINSIV5P-1DpBuhlm3ImNaSyW9y9-Nk");
//	    return geoApiContextBuilder.build();
//	}
}