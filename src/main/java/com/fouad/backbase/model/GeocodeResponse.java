package com.fouad.backbase.model;

import com.google.gson.Gson;

public class GeocodeResponse {

	private String formattedAddress;
	private String latitude;
	private String longitude;
	
	public GeocodeResponse() {}
	
	public GeocodeResponse(String formattedAddress, String latitude, String longitude) {
		this.formattedAddress = formattedAddress;
		this.latitude = latitude;
		this.longitude = longitude;
	}
	
	public String getFormattedAddress() {
		return formattedAddress;
	}
	public void setFormattedAddress(String formattedAddress) {
		this.formattedAddress = formattedAddress;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
	@Override
	public String toString(){
		Gson gson = new Gson();
		return gson.toJson(this);
	}
	
}
