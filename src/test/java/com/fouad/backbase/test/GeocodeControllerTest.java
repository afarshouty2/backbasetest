package com.fouad.backbase.test;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.assertNotNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.web.context.support.GenericWebApplicationContext;

import com.fouad.backbase.BackbaseTestApplication;
import com.fouad.backbase.model.GeocodeResponse;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.webAppContextSetup;


/**
 * @author Ahmed
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = BackbaseTestApplication.class)
@WebAppConfiguration
public class GeocodeControllerTest {

	@Autowired
	private GenericWebApplicationContext webApplicationContext;

	private MockMvc mockMvc;

	@Before
	public void getContext() {
		mockMvc = webAppContextSetup(webApplicationContext).build();
		assertNotNull(mockMvc);
	}

	@Test
	public void test1() throws Exception {
		GeocodeResponse expectedResultForParis = new GeocodeResponse("Paris, France", "48.8566140", "2.3522219");

		mockMvc.perform(get("/backbase/geocode?address=Paris").contentType(MediaType.APPLICATION_JSON)).andExpect(status().isOk())
				.andExpect(content().json(expectedResultForParis.toString()));
	}

}
