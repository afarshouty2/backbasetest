# Backbase Geocode Assignment #

The goal is to expose a JSON-based RESTful API, that will accept address as a single string and use this address
parameter to query Google API, that should be consumed using XML format. The JSON response should contain the
formatted address, as well as latitude and longitude of the address.

https://developers.google.com/maps/documentation/geocoding/intro

## Exposed Endpoints ##

* GET /backbase/geocode?address={Addreess}

Example 

```javascript
http://localhost:8080/backbase/geocode?address=Paris
```

Sample response 

```javascript
{
    "formattedAddress": "Paris, France",
    "latitude": "48.8566140",
    "longitude": "2.3522219"
}
```

### Requirements ###
* Java 8
* JUnit and Mockito for testing
* Maven as dependency package manager


### Compile, Test, Run, and Package ###
* Compile: mvn compile
* Test: mvn test
* Run: mvn spring-boot:run
* Packaging: mvn package


### Who do I talk to? ###

Ahmed Fouad 
ahmed.fouad.sayed@gmail.com